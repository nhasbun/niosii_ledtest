	niosii u0 (
		.clock_50_clk           (<connected-to-clock_50_clk>),           //        clock_50.clk
		.led_io_array_export    (<connected-to-led_io_array_export>),    //    led_io_array.export
		.reset_n_reset_n        (<connected-to-reset_n_reset_n>),        //         reset_n.reset_n
		.switch_io_array_export (<connected-to-switch_io_array_export>)  // switch_io_array.export
	);

