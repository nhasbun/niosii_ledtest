
module niosii (
	clock_50_clk,
	led_io_array_export,
	reset_n_reset_n,
	switch_io_array_export);	

	input		clock_50_clk;
	output	[7:0]	led_io_array_export;
	input		reset_n_reset_n;
	input	[7:0]	switch_io_array_export;
endmodule
