	pll u0 (
		.clock_50_clk         (<connected-to-clock_50_clk>),         //      clock_50.clk
		.pll_50_clk           (<connected-to-pll_50_clk>),           //        pll_50.clk
		.pll_50_locked_export (<connected-to-pll_50_locked_export>), // pll_50_locked.export
		.reset_n_reset_n      (<connected-to-reset_n_reset_n>),      //       reset_n.reset_n
		.pll_200_clk          (<connected-to-pll_200_clk>)           //       pll_200.clk
	);

