
module pll (
	clock_50_clk,
	pll_50_clk,
	pll_50_locked_export,
	reset_n_reset_n,
	pll_200_clk);	

	input		clock_50_clk;
	output		pll_50_clk;
	output		pll_50_locked_export;
	input		reset_n_reset_n;
	output		pll_200_clk;
endmodule
