/**
Descripcion,
Modulo debouncer generico

-----------------------------------------------------------------------------
Author : Nicolas Hasbun
File   : debouncer.v
Create : 2017-11-01 05:16:35
Editor : sublime text3, tab size (2)
-----------------------------------------------------------------------------
*/

module debouncer(
  input clock50,
  input signal_in,
  output reg signal_out
);

initial signal_out = 0;

parameter VALORMAX = 32'd100_000;
integer contador; initial contador = 0;

always @(posedge clock50) begin
  if (contador > VALORMAX)
    contador <= 0;
  else
    contador <= contador + 1'b1;
end

// SYNC CHAIN, signal_in senal async
reg signal_in_1; initial signal_in_1 = 0;
reg signal_in_2; initial signal_in_2 = 0;
always @(posedge clock50) signal_in_1 <= signal_in;
always @(posedge clock50) signal_in_2 <= signal_in_1;

always @(posedge clock50) begin
  if(contador == 0)
    signal_out <= signal_in_2;
end

endmodule