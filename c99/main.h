#include <unistd.h> // usleep()
#include <system.h>
#include <stdint.h> // uints types
#include <stdbool.h>

#include "sys/alt_stdio.h"
#include "sys/alt_irq.h" // sistema de interrupciones
#include "altera_avalon_pio_regs.h" // **
// funciones IOWR_ALTERA_AVALON_PIO_IRQ_MASK()
//           IORD_ALTERA_AVALON_PIO_EDGE_CAP()

#define BIT(x,n) (((x) >> (n)) & 1)

void init_interrupcion();
void interrupt_handler(void * isr_context);
void aplicacion(uint8_t switch_id);