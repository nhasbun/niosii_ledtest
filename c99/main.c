#include "main.h"

#ifdef LED_IO_BASE
#elif SWITCH_IO_BASE
#endif

#define DELAY 100000

volatile int flanco; // **
// flanco detecta un IRQ generado por uno de los switchs en forma
// de mascara.
// flanco funciona fuera del contexto del userspace
// de algunas funciones, esto debe considerarlo el compilador. Sin esto
// un cambio en el valor de la variable fuera de la funcion puede ser
// ignorada por procesos de optimizacion (en el contexto de una sola
// funcion el valor de esta variable puede no cambiar).

int main()
{
	init_interrupcion();
	volatile uint8_t * led_p = (uint8_t*)(LED_IO_BASE); // **
	// se declara LED_IO_BASE como puntero de uint8_t

	*led_p = 1;
	bool multiplicar = true;

	while (1)
	{
	  uint8_t valor_led = *led_p; // **
	  // como es volatile, una pequeña optimizacion es no leer el valor del led
	  // de forma constante y solo leer una vez y luego escribir en su registro

	  // Rutina para mover los LEDS
	  if (multiplicar)
	  	valor_led = valor_led << 1;
	  else
	  	valor_led = valor_led >> 1;

	  *led_p = valor_led;

	  // Rutina para cambiar de direccion
	  if (valor_led == 128)
	  	multiplicar = false; // se comienza a dividir
	  if (valor_led == 1)
	  	multiplicar = true; // se vuelve a multiplicar

	  usleep(DELAY);
	}
  return 0;
}

void init_interrupcion()
{
	void * flanco_p = (void*) &flanco; // solo para cumplir con el prototipe
	// de alt_ic_isr_register()

	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(SWITCH_IO_BASE, 0xf); // **
	// Activa las interrupciones
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(SWITCH_IO_BASE, 0x0); // **
	// Reset edge capture register

	alt_ic_isr_register(
		SWITCH_IO_IRQ_INTERRUPT_CONTROLLER_ID,
		SWITCH_IO_IRQ,
		interrupt_handler,
		flanco_p,
		0); // retorna un entero
}

void interrupt_handler(void * isr_context)
{
	volatile int * flanco_p = (volatile int*) isr_context;

	*flanco_p = IORD_ALTERA_AVALON_PIO_EDGE_CAP(SWITCH_IO_BASE);
	// se lee cual switch genero el flanco
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(SWITCH_IO_BASE, 0x0);
	// reset a los valores de cambio de flanco (la senal ya fue tratada)
	aplicacion((uint8_t)(*flanco_p));

	IORD_ALTERA_AVALON_PIO_EDGE_CAP(SWITCH_IO_BASE);
	// pequeño delay para prevenir errores sugerido en la documnetacion
	// ni idea que hace :P
}

void aplicacion(uint8_t switch_id)
{
	uint8_t i = 0;
	while(1) {
		uint8_t bit = BIT(switch_id, i);
		if (bit == 1) break;
		i++;
	}
	alt_printf("Interrupcion detectada en switch %x \n", i);
}
